ruleset sensor_profile {
  meta {
    shares __testing, retrieve_profile
    provides retrieve_profile, threshold, sms
  }
  global {
    __testing = { "queries":
      [ { "name": "__testing" }
      //, { "name": "entry", "args": [ "key" ] }
      ] , "events":
      [
        {"domain": "sensor", "type": "profile_updated",
          "attrs": ["name", "location", "threshold", "sms"]}
      ]
    }

    retrieve_profile = function() {
      {
        "name": ent:name.defaultsTo(""),
        "location": ent:location.defaultsTo(""),
        "threshold": ent:threshold.defaultsTo("78"),
        "sms": ent:sms_number.defaultsTo("")
      }
    };
    threshold = function() {
      retrieve_profile(){"threshold"}
    };
    sms = function() {
      retrieve_profile(){"sms"}
    };
  }

  rule update_profile {
    select when sensor profile_updated
    pre {
      preset_values = retrieve_profile();
    }
    // we can't return the updated values in the response because entity
    //   variables can only be updated in the postlude
    send_directive("update profile", {"message": "processed"});
    always {
      ent:location := event:attr("location").defaultsTo(preset_values{"location"});
      ent:name := event:attr("name").defaultsTo(preset_values{"name"});
      ent:threshold := event:attr("threshold").defaultsTo(preset_values{"threshold"});
      ent:sms_number := event:attr("sms").defaultsTo(preset_values{"sms"});
    }
  }
}
