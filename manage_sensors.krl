ruleset manage_sensors {
  meta {
    shares __testing, sensors, all_sensor_records
    provides sensors, all_sensor_records, default_threshold
    use module io.picolabs.wrangler alias Wrangler
  }
  global {
    __testing = { "queries":
      [ { "name": "__testing" },
        {"name": "sensors"},
        {"name": "all_sensor_records"}
      ] , "events": [
        {"domain": "sensor", "type": "new_sensor", "attrs": ["name", "profile_name"]},
        {"domain": "sensor", "type": "unneeded_sensor", "attrs": ["name"]},
        {"domain": "sensor", "type": "delete_sensors"}
      ]
    }
    
    sensors = function() {
      ent:sensors.defaultsTo({})
    }
    
    all_sensor_records = function() {
      // get all the temperature readings from each child sensor
      ent:sensors.defaultsTo({}).map(function(value, key) {
        Wrangler:skyQuery(value{"eci"}, "temperature_store", "temperatures")
      })
      
    }
    
    default_threshold = function() {
      78
    }
    default_sms = "+18018301745"
  }
  
  rule create_sensor {
    select when sensor new_sensor
    pre {
      sensor_name = event:attr("name")
      exists = ent:sensors >< sensor_name
    }
    if not exists then
      noop()
    fired {
      raise wrangler event "child_creation"
        attributes { "name": sensor_name,
                     "color": "#00ffff",
                     "profile_name": event:attr("profile_name"),
                     "rids": ["temperature_store", "wovyn_base", "sensor_profile", "post_test"]}
    }
  }
  
  rule check_duplicate_sensor {
    select when sensor new_sensor
    pre {
      sensor_name = event:attr("name")
      exists = ent:sensors >< sensor_name
    }
    if exists then
      send_directive("error", {"message": "sensor '" + sensor_name + "' already exists"})
  }
  
  rule new_sensor_initialized {
    select when wrangler child_initialized
    pre {
      sensor_name = event:attr("name")
      sensor_eci = event:attr("eci")
    }
    event:send({
      "eci": sensor_eci, 
      "eid": "child-initialization",
      "domain": "sensor", 
      "type": "profile_updated",
      "attrs": {
          "name": event:attr("rs_attrs"){"profile_name"}, 
          "threshold": default_threshold(), 
          "sms": default_sms}
    })
    fired {
      ent:sensors := ent:sensors.defaultsTo({});
      // Note that we save the sensor name again inside the object; this simplifies passing it to the wrangler
      ent:sensors{[sensor_name]} := {"name": sensor_name, "id": event:attr("id"), "eci": sensor_eci};
    }
  }
  
  rule delete_sensor {
    select when sensor unneeded_sensor
    pre {
      sensor_name = event:attr("name")
      exists = ent:sensors >< sensor_name
    }
    if exists then
      send_directive("deleting sensor", {"sensor_name": sensor_name})
    fired {
      raise wrangler event "child_deletion"
        attributes {"name": sensor_name};
      clear ent:sensors{[sensor_name]}
    }
  }
  
  rule clear_manager {
    select when sensor delete_sensors
      foreach ent:sensors setting (value, key)
    always {
      raise sensor event "unneeded_sensor"
        attributes {"name": key}
    }
  }
}
